# Ucuncu Parti Paketler
---

1. Asset store'daki Virtual Joystick Pack'i ekranin her yerinde hareket algilamak icin kullandim.  
   Paketteki joystick spritelarinin Alphalarini sifir olarak ayarlayip gorunmez hale getirdim.  
   Movement scriptinde joystickteki degerleri ve belirledigim bir "damp" degerini kullanarak donuslerin ani olmasini engelledim.
2. Simple Health Bar paketinden 2 adet sprite kullandim(healthbar arkaplani ve ici).  
3. Grids & Nets texture Pack'ten plane icin bi adet material kullandim.


# Mekanikler
---
### Hareket
Her karakterin 1 basi, 3 unit kuyrugu ve boynuzu var.  
Hareketler x ve z koordinatlarinda pozisyon degistirerek saglaniyor, y koordinati 1e sabit.  
Her kuyruk unit'i hiyerarside kendisinden bir onceki objeyi takip ediyor.

### Level ve Collectiblelar
Botlarin ve oyuncunun birbirine cok benzer 2 level scripti var.  
Tek farklari oyuncununkinde UI degiskenleri olmasi.
Karakterler 1 leveldan basliyor, maksimum level 3.  
Her levelda boynuz sayisi ve sekilleri degisiyor.  
Yeni level icin gereken experience her levelda 2 katina cikiyor, 100 olarak basliyor.  
Haritadaki mor collectible objeler 25 exp veriyor, baska bir karakteri oldurmek 50 exp veriyor.  
Haritadaki sari collectible objeler 1 saniyeligine hizi 2 katina cikariyor.  
Ayrica her karakter level atladiginda ve boynuzu degistiginde SelfCollision scripti calisiyor.  
Bu script karakterlerin kendi booynuzlari tarafindan olmelerini engelliyor.

### Botlar
Botlar dogduklarinda haritada rastgele bir noktayi hedef secip oraya ilerlemeye basliyorlar.  
Hedefe varmadan once rastgele bi intervalde yeni bir hedef belirleyip yon degistiriyorlar.  
Botlar olustuklarinda agresif basliyorlar( belli bir mesafedeyse oyuncuyu hedef aliyorlar ) fakat bu durum her yeni pozisyon secildiginde rastgele degisiyor.   
Bot yeni pozisyon sectiginde degeri pasif olarak ayarlanirsa oyuncu belli bir mesafedeyken ondan kacmayi tercih ediyorlar.  
Botlar tekrar olustugunda levellari sifirlaniyor.

### Pooling
Cok basit bir pooling scripti yazdim.  
Script verdiginiz objeden istediginiz miktarda bir List olusturuyor, bu listedeki objeleri oyun basladiginda yaratip hiyerarside inaktif hale getiriyor.  
Sonra belirli araliklarla aktive ediyor.
  
### Collisionlar
Collisionlar her karakterin boynuzunda tespit ediliyor.  
Boynuz baska bir karakterin "body" tagli bir objesine degerse diger karakter inaktif ediliyor.  

### GUI
Test icin sol uste Respawn tusu ekledim, son olunen noktada tekrar dogmayi sagliyor.