﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfCollision : MonoBehaviour
{

    void Awake (){
        RemoveSelfCollisions();
    }

    void RemoveSelfCollisions(){
         Collider [] colliders = GetComponentsInChildren<Collider>();
        for (int i = 0; i < colliders.Length; i++) {
            for (int j = i; j < colliders.Length; j++){
                Physics.IgnoreCollision(colliders[i], colliders[j]);
            }
        }
    }

}
