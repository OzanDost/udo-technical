﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerLevelManager _PLM;
    public Movement _PlayerMovement;
    
    private void Start()
    {
        _PLM = gameObject.GetComponent<PlayerLevelManager>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "bait")
        {
            _PLM.SendMessage("GainExperience", 25);
            collision.gameObject.SetActive(false);
        }
        if (collision.collider.tag == "speedUp")
        {
            StartCoroutine(Dash());
            collision.gameObject.SetActive(false);
        }
        
    }

    IEnumerator Dash()
    {
        _PlayerMovement.moveSpeed = 24;
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(.2f);

        }
        _PlayerMovement.moveSpeed = 12;
        StopCoroutine(Dash());
    }
}
