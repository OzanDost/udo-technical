﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HornHit : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerLevelManager PLM;
    public BotLevelManager BLM;

    void Start()
    {
        try
        {
            PLM = GetComponentInParent<PlayerLevelManager>();
        }
        catch (System.Exception)
        {
            Debug.Log("this is a bot");
        }

        try
        {
            BLM = GetComponentInParent<BotLevelManager>();
        }
        catch (System.Exception)
        {
            
            Debug.Log("This is player");
        }
        
    }


    private void OnCollisionEnter(Collision other) {
        if(other.collider.tag == "body")
        {   
            if(PLM != null)
            {
                PLM.SendMessage("GainExperience", 50);
            }
            else if(BLM != null)
            {
                BLM.SendMessage("GainExperience", 50);
            }
            
            other.gameObject.transform.parent.gameObject.SetActive(false);
        }
    }
}
