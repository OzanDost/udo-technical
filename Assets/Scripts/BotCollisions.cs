using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotCollisions : MonoBehaviour
{
    public WanderInArea _WIA;
    
    private void Start()
    {
        _WIA = gameObject.GetComponent<WanderInArea>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "bait")
        {
            
            collision.gameObject.SetActive(false);
        }
        if(collision.collider.tag == "speedUp")
        {
            StartCoroutine(Dash());
            collision.gameObject.SetActive(false);
        }
    }
    IEnumerator Dash()
    {
        
        for(int i =0; i<3; i++)
        {
            
            yield return new WaitForSeconds(.2f);
            
        }
        
        StopCoroutine(Dash());
    }
}
