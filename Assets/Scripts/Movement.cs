﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update

    public float moveSpeed;
    public Joystick joystick;
    [SerializeField]
    public float rotationDamp;
    public Transform mainCamera;

    private void Start()
    {
        moveSpeed = 12f;
    }
    private void Update()
    {
        Vector3 moveVector = (Vector3.right * joystick.Horizontal + Vector3.forward * joystick.Vertical);

        if (moveVector != Vector3.zero)
        {
            PlayerRotate(moveVector);
            PlayerMove(moveVector);
        }

        CameraFollow();
    }

    [SerializeField]
    public float cameraOffset;
    void CameraFollow()
    {
        Vector3 cameraVelocity = Vector3.zero;
        mainCamera.position = Vector3.SmoothDamp(mainCamera.position, 
                                                new Vector3(gameObject.transform.position.x , mainCamera.position.y, gameObject.transform.position.z -cameraOffset),
                                                ref cameraVelocity, .1f);
    }

    void PlayerRotate(Vector3 target)
    {
        Quaternion rotateDirection = Quaternion.LookRotation(target);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateDirection, rotationDamp * Time.deltaTime);

    }

    void PlayerMove(Vector3 target)
    {
        transform.Translate(target.normalized * moveSpeed * Time.deltaTime, Space.World);
    }
}
