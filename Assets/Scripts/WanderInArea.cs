using UnityEngine; 
using System.Collections; 

public class WanderInArea:MonoBehaviour {
    public GameObject moveArea; 
    public BotLevelManager _BLM; 
    public float speed; 
    public float randomX; 
    public float randomZ; 
    private Vector3 currentRandomPos; 
    public float distance; 
    public GameObject player;
    public bool fightOrFlight;

    private void Awake() {
        player = GameObject.FindGameObjectWithTag("player").transform.GetChild(0).gameObject;
        moveArea = GameObject.FindGameObjectWithTag("MoveArea"); 
        _BLM = gameObject.GetComponentInChildren<BotLevelManager>(); 
    }
    private void Start() {

        fightOrFlight = true;
        speed = 8f; 
        randomX = moveArea.transform.GetChild(0).transform.position.x; 
        randomZ = moveArea.transform.GetChild(1).transform.position.z; 
        
        PickNewPosition(); 
    }

    private void Update() {

        if(Vector3.Distance(player.transform.position, transform.position) < 10f  && player.activeInHierarchy)
        {
            AttackOrRun();   
        }
        else
        {
            float destinationDistance = Vector3.Distance(transform.position, currentRandomPos);
            //Debug.Log(destinationDistance);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(currentRandomPos - transform.position), 5 * Time.deltaTime); 
            transform.position += transform.forward * speed * Time.deltaTime;
            
            if (destinationDistance <= Random.Range(.2f, distance/4)) {
                //Debug.Log(gameObject.transform.parent.name+ "has changed direction when " + destinationDistance + " left");
                PickNewPosition(); 
            }
        }
    }

    private void OnCollisionEnter(Collision other) {
        if (other.collider.tag == "speedUp") {

            other.gameObject.SetActive(false); 
            StartCoroutine(SpeedUp()); 
        }
        if (other.collider.tag == "bait") {

            other.gameObject.SetActive(false);
            _BLM.SendMessage("GainExperience",25);
        }
    }

    IEnumerator SpeedUp() {
        speed = 16f; 
        for (int i = 0; i < 3; i++) {
            yield return new WaitForSeconds(.2f); 
        }
        speed = 8f; 
        StopCoroutine(SpeedUp()); 
    }

    void PickNewPosition() {

        currentRandomPos = new Vector3(Random.Range( - randomX, randomX), 1, Random.Range( - randomZ, randomZ)); 
        distance = Vector3.Distance(transform.position, currentRandomPos); 
        
        //directionChangeInterval = distance/speed - 1f; 
        WillFightOrFlee();
    }

    void WillFightOrFlee(){
        int decider = Random.Range(-1,1);
        if(decider >=0){
            fightOrFlight = true;
        }
        else{
            fightOrFlight = false;
        }
    }

    void AttackOrRun(){
        
        Vector3 playerPos = new Vector3(player.transform.position.x, 1, player.transform.position.z);

        if(fightOrFlight)
        {

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(playerPos - transform.position), 5 * Time.deltaTime); 
                        
            transform.position += transform.forward * speed * Time.deltaTime;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(transform.position - playerPos), 5 * Time.deltaTime); 
                        
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        
    }
}