﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    // Start is called before the first frame update
    public ObjectPooler OP;
    private float randomX, randomZ;
    void Start()
    {
        randomX = gameObject.transform.GetChild(0).position.x;
        randomZ = gameObject.transform.GetChild(1).position.z;
        InvokeRepeating("CreateCollectibles", .5f, .5f);
    }

    void CreateCollectibles()
    {
        GameObject collectible = OP.GetPooledObject();

        if (collectible == null) return;
  
        if(collectible.transform.childCount !=0){
            
            for (int i = 0; i < collectible.transform.childCount; i++){
                collectible.transform.GetChild(i).localPosition = new Vector3(0,0,0);
                
            }
        }
        collectible.transform.position = new Vector3(Random.Range(-randomX,randomX),1,Random.Range(-randomZ,randomZ));
        collectible.SetActive(true);
        
        
    }
}
