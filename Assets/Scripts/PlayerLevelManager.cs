﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLevelManager : MonoBehaviour
{
    public int currentLevel;
    public float nextLevelExp;
    public float currentExp;
    public Text levelDisplay;
    public Image levelBarDisplay;
    public GameObject [] horns;
    private SelfCollision SC;
    
    private void Awake() {
        SC = GetComponentInParent<SelfCollision>();
    }
    void Start()
    {
        currentLevel = 1;
        levelDisplay.text = currentLevel.ToString();
        nextLevelExp = 100;
        currentExp = 0;
        
        AdjustHorns();
    }

    private void OnEnable() {
        currentLevel = 1;
        levelBarDisplay.fillAmount = 0;
        levelDisplay.text = currentLevel.ToString();
        nextLevelExp = 100;
        currentExp = 0;
        AdjustHorns();
       
    }

    void GainExperience(float expAmount)
    {
        currentExp += expAmount;
        levelBarDisplay.fillAmount += expAmount/nextLevelExp;
        Debug.Log(currentExp);
        LevelUp();
    }

    void AdjustHorns(){
        foreach(GameObject horn in horns){
                horn.SetActive(false);
            }
            if(currentLevel<3){
                horns[currentLevel-1].SetActive(true);
            }
            else{
                horns[horns.Length-1].SetActive(true);
            }

            SC.SendMessage("RemoveSelfCollisions");
    }
    
    void LevelUp()
    {
        if(currentExp >= nextLevelExp)
        {
            nextLevelExp *= 2;
            currentExp = 0;
            currentLevel++;
            levelDisplay.text = currentLevel.ToString();
            levelBarDisplay.fillAmount = 0;

            AdjustHorns();
        }

    }
}
