﻿using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 

public class BotLevelManager:MonoBehaviour {
    public int currentLevel; 
    public float nextLevelExp; 
    public float currentExp; 
    public GameObject[] horns;
    private SelfCollision SC;
    
    private void Awake() {
        SC = GetComponentInParent<SelfCollision>();
    }
    // Start is called before the first frame update
    void Start() {
        currentLevel = 1; 
        nextLevelExp = 100; 
        currentExp = 0;
        AdjustHorns();
        //trail.emitting = true;
    }

    private void OnEnable() {
        currentLevel = 1;
        currentExp = 0;
        nextLevelExp = 100;
        AdjustHorns();
        
    }
    
    private void Update() {
        if(!gameObject.activeInHierarchy){
            
        }
    }

    // Update is called once per frame
    void GainExperience(float expAmount) {
        currentExp += expAmount; 
        LevelUp(); 
    }

    void AdjustHorns(){
        foreach (GameObject horn in horns) {
                horn.SetActive(false); 
            }
            if(currentLevel<3){
                horns[currentLevel-1].SetActive(true);
            }
            else{
                horns[horns.Length-1].SetActive(true);
            }

            SC.SendMessage("RemoveSelfCollisions");
    }

    void LevelUp() {
        if (currentExp >= nextLevelExp) {
            nextLevelExp *= 2; 
            currentExp = 0; 
            currentLevel++;
            AdjustHorns();

        }
    }

    
}
