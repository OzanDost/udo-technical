﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartMovement : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform bodyContainer;
    public Transform [] _bodyParts;
    void Start()
    {
        _bodyParts = new Transform[bodyContainer.childCount];
        for(int i =0; i<bodyContainer.childCount; i++){
            _bodyParts[i] = bodyContainer.GetChild(i);
        }
    }
    // Update is called once per frame
    void Update()
    {
        FollowHead();
    }
  
    [Range(-.5f,2f)]
    public float overTime = 0.5f;
    [SerializeField]
    float minDistance = 1.7f;
    Vector3 diff;
    void FollowHead()
    {
        int index = transform.GetSiblingIndex();
        diff = gameObject.transform.position - _bodyParts[index-1].transform.position;
        diff.y = 0.0f;
        
        transform.position = _bodyParts[index-1].transform.position + diff.normalized * minDistance;
        //transform.position = Vector3.SmoothDamp(transform.position, diff.normalized * minDistance, ref movementVelocity, overTime);
        transform.LookAt(_bodyParts[index-1].transform.position);

    }
}
